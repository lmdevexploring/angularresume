# "Stark" Angular Resume

This project pretends to be a resume builder and has its own template. For me it is a way to put some new learning in practice.
Feel free to Pull, Fork or do whatever you like with it! Grateful though if you mention this work while you're in the way.

Bitbucket/Firebase CI/CD publishes this to [https://ironmanresume.web.app/](https://ironmanresume.web.app/) and [https://ironmanresume.firebaseapp.com/](https://ironmanresume.firebaseapp.com/). Check it out!


## Development server

Run `ng serve` or `ng serve --open` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).


## Timeline design inspiration from
https://codepen.io/ArnaudBalland/pen/rLeQgd/

import { Component, OnInit } from '@angular/core';
import personalData from 'src/assets/personaldata.json';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  personalData = personalData;

  constructor() { }

  ngOnInit() {
  }

}

import timelineData from 'src/assets/lifelinedata.json';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css']
})


export class TimelineComponent implements OnInit {

  timelineData = timelineData;
  filteredTimelineData: any[];
  timelineEventTypes = [];

  // timelineComponentFilter: TimelineComponentFilter;

  timelineEventTypeSelectChange(i) {
    this.timelineEventTypes[i].selected = !this.timelineEventTypes[i].selected;
    console.log(this.timelineEventTypes);
    this.applyTimelineEventTypeFilter();
  }

  constructor() { }

  private applyTimelineEventTypeFilter() {
    const selectedTimelineEventFilterTypes = this.timelineEventTypes.filter(f => f.selected);
    this.filteredTimelineData = timelineData.filter(f => selectedTimelineEventFilterTypes.some(p => p.timelineEvent === f.type));
    this.filteredTimelineData = this.filteredTimelineData.sort((a, b) => {
      return ((new Date(b.dateStart) as any) - (new Date(a.dateStart) as any))});

  }

  ngOnInit() {

    const timelineEventTypesTemp = [];

    timelineData.forEach(timelineEvent => {
      if (timelineEventTypesTemp.filter(f => f.timelineEvent === timelineEvent.type).length === 0) {
        timelineEventTypesTemp.push({timelineEvent: timelineEvent.type, selected: true});
      }
    });

    this.timelineEventTypes = timelineEventTypesTemp.sort((one, two) => (one.timelineEvent > two.timelineEvent ? 1 : -1));
    this.applyTimelineEventTypeFilter();

  }

}
